Cerinte
Proiectati si implementati o aplicatie Java pentru rezolvarea problemei propuse. Se va evidentia o arhitectura stratificata. Informatiile vor fi preluate din fisiere text.

3. Agenda personala
	Se cere dezvoltarea unei aplicatii care sa sprijine utilizatorii in organizarea eficienta a timpului si a relatiilor sociale. Aplicatia va avea urmatoarele functionalitati:
		i. adaugarea de contacte (nume, adresa, numar de telefon);
		ii. programarea unor activitati (numele utilizatorului care a creat activitatea, descriere, data, locul, ora inceput, data sfarsit, ora sfarsit, lista contactelor);
		iii. generarea unui raport cu activitatile create de utilizator (nume, user, parola) la o anumita data, ordonate dupa ora de inceput.
		