package agenda.controller;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.model.repository.interfaces.RepositoryUser;

import java.util.Date;
import java.util.List;

public class AgendaController {
    private RepositoryContact contactRep;
    private RepositoryUser userRep;
    private RepositoryActivity activityRep;

    public AgendaController() throws Exception {
        contactRep = new RepositoryContactFile();
        userRep = new RepositoryUserFile();
        activityRep = new RepositoryActivityFile(contactRep);
    }

    public void addContact(Contact contact)
    {
        this.contactRep.addContact(contact);
    }

    public boolean addActivity(Activity activity)
    {
        return this.activityRep.addActivity(activity);
    }


    public List<Activity> activitiesOnDate(String name, Date d) {
        return this.activityRep.activitiesOnDate(name, d);
    }

    public User getByUsername(String u) {
        return this.userRep.getByUsername(u);
    }
}
