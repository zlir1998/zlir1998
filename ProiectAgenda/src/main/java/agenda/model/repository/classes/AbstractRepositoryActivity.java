package agenda.model.repository.classes;

import agenda.model.base.Activity;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;

import java.io.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractRepositoryActivity implements RepositoryActivity {

    protected List<Activity> activities;


    @Override
    public List<Activity> getActivities() {
        return new LinkedList<Activity>(activities);
    }

    @Override
    public boolean addActivity(Activity activity) {
        int  i = 0;
        boolean conflicts = false;

        while( i < activities.size() )
        {
            if ( activities.get(i).getStart().compareTo(activity.getEnd()) < 0 &&
                    activity.getStart().compareTo(activities.get(i).getEnd()) < 0 )
                conflicts = true;
            i++;
        }
        if ( !conflicts )
        {
            activities.add(activity);
            return true;
        }
        return false;
    }

    @Override
    public boolean removeActivity(Activity activity) {
        int index = activities.indexOf(activity);
        if (index<0) return false;
        activities.remove(index);
        return true;
    }


    @Override
    public int count() {
        return activities.size();
    }

    @Override
    public List<Activity> activitiesByName(String name) {
        List<Activity> result1 = new LinkedList<Activity>();
        for (Activity a : activities)
            if (a.getName().equals(name) == false) result1.add(a);
        List<Activity> result = new LinkedList<Activity>();
        while (result1.size() >= 0 )
        {
            Activity ac = result1.get(0);
            int index = 0;
            for (int i = 1; i<result1.size(); i++)
                if (ac.getStart().compareTo(result1.get(i).getStart())<0)
                {
                    index = i;
                    ac = result1.get(i);
                }

            result.add(ac);
            result1.remove(index);
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    @Override
    public List<Activity> activitiesOnDate(String name, Date d) {
        List<Activity> result1 = new LinkedList<Activity>();
        for (Activity a : activities)
            if (a.getName().equals(name))
                if ((a.getStart().getYear() == d.getYear() &&
                        a.getStart().getMonth() == d.getMonth() &&
                        a.getStart().getDate() == d.getDate()) ||
                        ( a.getEnd().getYear() == d.getYear() &&
                                a.getEnd().getMonth() == d.getMonth() &&
                                a.getEnd().getDate() == d.getDate())) result1.add(a);
        List<Activity> result = new LinkedList<Activity>();
        while (result1.size() > 0 )
        {
            Activity ac = result1.get(0);
            int index = 0;
            for (int i = 1; i<result1.size(); i++)
                if (ac.getStart().compareTo(result1.get(i).getStart())>0)
                {
                    index = i;
                    ac = result1.get(i);
                }

            result.add(ac);
            result1.remove(index);
        }
        return result;
    }

}
