package agenda.model.repository.classes;

import java.util.LinkedList;
import java.util.List;

import agenda.model.base.Contact;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.exceptions.InvalidFormatException;

public class RepositoryContactMock extends AbstractRepositoryContact {

	
	public RepositoryContactMock() {
		contacts = new LinkedList<Contact>();
	}

	@Override
	public boolean saveContracts() {
		return true;
	}
}
