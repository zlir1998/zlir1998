package agenda.model.repository.classes;


import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import agenda.model.repository.interfaces.RepositoryContact;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractRepositoryContact implements RepositoryContact {

    protected List<Contact> contacts;

    @Override
    public List<Contact> getContacts() {
        return new LinkedList<Contact>(contacts);
    }

    @Override
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    @Override
    public boolean removeContact(Contact contact) {
        int index = contacts.indexOf(contact);
        if (index < 0)
            return false;
        else
            contacts.remove(index);
        return true;
    }

    @Override
    public int count() {
        return contacts.size();
    }

    @Override
    public Contact getByName(String string) {
        for (Contact c : contacts)
            if (c.getName().equals(string))
                return c;
        return null;
    }

    @Override
    public void addContact(String name, String address, String telephone) throws InvalidFormatException {
        this.addContact(new Contact(name, address, telephone));
    }
}
