package agenda.model.repository.classes;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RepositoryContactMockTest {

    AbstractRepositoryContact contactRepository;
    private static String invalidFormatExceptionMessage = "Cannot convert";
    private static String invalidFormatExceptionTelephoneReason = "Invalid phone number";
    private static String invalidFormatExceptionNameReason = "Invalid name";
    private static String invalidFormatExceptionAddressReason = "Invalid address";


    @Before
    public void setUp() throws Exception {
        contactRepository = new RepositoryContactMock();
    }

    private void addSuccessful(String name, String address, String telephone) {
        boolean isFound;
        try {
            contactRepository.addContact(name, address, telephone);
            assertTrue(true);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }

        isFound = false;
        for (Contact c : contactRepository.getContacts()) {
            if (c.getName().equals(name) && c.getAddress().equals(address) && c.getTelefon().equals(telephone)) {
                isFound = true;
            }
        }

        assertTrue(isFound);
    }

    @Test
    public void tc1_ec() {
        String name = "Ion";
        String address = "address of Ion";
        String telephone = "12345";
        addSuccessful(name, address, telephone);
    }

    @Test
    public void tc3_ec() {
        addSuccessful("", "address of Ion", "+00511515");
    }

    @Test
    public void tc7_ec() {
        addSuccessful("Ion Ion", "address of Ion", "197263");
    }

    private void addInvalid(String name, String address, String telephone, String expectedMessage, String expectedReason) {
        Integer count = contactRepository.getContacts().size();
        try {
            contactRepository.addContact(name, address, telephone);
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage().equals(expectedMessage));
            assertTrue(e.getCause().getMessage().equals(expectedReason));
        }

        assertTrue(contactRepository.getContacts().size() == count);
    }

    @Test
    public void tc5_ec() {
        String name = "Ion";
        String address = "address of Ion";
        String telephone = "+01aaa";

        addInvalid(name, address, telephone, invalidFormatExceptionMessage, invalidFormatExceptionTelephoneReason);
    }

    @Test
    public void tc4_ec() {
        addInvalid(
                "Unu doi.trei",
                "address of Ion",
                "+0145",
                invalidFormatExceptionMessage,
                invalidFormatExceptionNameReason
        );
    }

    @Test
    public void tc6_bva() {
        addSuccessful("Ion", "", "0129");
    }

    @Test
    public void tc5_bva() {
        addInvalid("Ion.Ion......Ion", "address", "0123", invalidFormatExceptionMessage, invalidFormatExceptionNameReason);
    }

    @Test
    public void tc8_bva(){
        addSuccessful("Ion", "D", "01923");
    }


    @Test
    public void tc12_bva() {
        addInvalid("Ion", "", "", invalidFormatExceptionMessage, invalidFormatExceptionTelephoneReason);
    }

    @Test
    public void tc3_bva(){
        addSuccessful("Ion", "", "01923");
    }
}