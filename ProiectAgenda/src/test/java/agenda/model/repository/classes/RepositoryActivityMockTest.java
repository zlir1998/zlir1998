package agenda.model.repository.classes;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class RepositoryActivityMockTest {

    RepositoryActivityMock activityRepository ;
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");

    @Before
    public void setUp() throws Exception {
        activityRepository = new RepositoryActivityMock();
    }


    @Test
    public void req02_tc01() throws ParseException {
        activityRepository.setActivities(new ArrayList<Activity>());
        Activity activity = new Activity("A1",
                dateFormat.parse("12/21/2012 21:21"),
                dateFormat.parse("12/21/2012 21:22"),
                new ArrayList<Contact>(),
                "description");

        assertTrue(activityRepository.addActivity(activity));
    }

    @Test
    public void req02_tc05()
    {
        ArrayList<Activity> activities = new ArrayList<Activity>();
        activities.add(null);
        activityRepository.setActivities(activities);
        Activity activity = null;

        try {
            activityRepository.addActivity(activity);
            assertTrue(false);
        }
        catch (NullPointerException ex)
        {
            assertTrue(true);
        }
    }

    @Test
    public void req02_tc04() throws ParseException {
        ArrayList<Activity> activities = new ArrayList<Activity>();
        activities.add(new Activity("A1",
                dateFormat.parse("10/21/2012 19:21"),
                dateFormat.parse("12/21/2012 20:00"),
                new ArrayList<Contact>(),
                "description"));
        activityRepository.setActivities(activities);
        Activity activity = new Activity("A3",
                dateFormat.parse("11/21/2012 20:21"),
                dateFormat.parse("11/21/2012 21:20"),
                new ArrayList<Contact>(),
                "description");

        assertFalse(activityRepository.addActivity(activity));
    }

    @Test
    public void req02_tc02() throws ParseException {
        ArrayList<Activity> activities = new ArrayList<Activity>();
        activities.add(new Activity("A1",
                dateFormat.parse("12/21/2012 21:21"),
                dateFormat.parse("12/21/2012 22:22"),
                new ArrayList<Contact>(),
                "description"));
        activityRepository.setActivities(activities);
        Activity activity = new Activity("A1",
                dateFormat.parse("12/21/2012 20:21"),
                dateFormat.parse("12/21/2012 21:20"),
                new ArrayList<Contact>(),
                "description");

        assertTrue(activityRepository.addActivity(activity));
    }

}